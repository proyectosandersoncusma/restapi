package com.mitocode.service.impl;

import com.mitocode.model.Patient;
import com.mitocode.repo.IPatientRepo;
import com.mitocode.service.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientServiceImpl implements IPatientService {

    @Autowired
    private IPatientRepo repo;// = new PatientRepoImpl();

    @Override
    public Patient save(Patient patient) {
        return repo.save(patient);
    }

    @Override
    public Patient update(Patient patient) {
        return repo.save(patient);
    }

    @Override
    public List<Patient> findAll() {
        return repo.findAll();
    }

    @Override
    public Patient findById(Integer id) {
        Optional<Patient> op = repo.findById(id);
        //return op.isPresent() ? op.get() : new Patient();
        return op.orElse(null);//Patient::new
    }

    @Override
    public void delete(Integer id) {
        repo.deleteById(id);
    }
}
